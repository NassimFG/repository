﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MySql.Data.MySqlClient;

namespace SYNAPSINFO
{
    public class Intervenant
    {
        private int _idIntervenant;
        private string _nom;
        private decimal _tauxHoraire;

        public decimal TauxHoraire
        {
            get
            {
                return _tauxHoraire;
            }
        }

        public Intervenant()
        {
            _idIntervenant = -1;
        }

        #region Champs à portée classe contenant l'ensemble des requêtes d'accès aux données

        private static string _selectSql = "SELECT idIntervenant, nom, tauxHoraire FROM intervenant";

        private static string _selectByIdSql = "SELECT idIntervenant, nom, tauxHoraire FROM intervenant WHERE id = ?id";

        private static string _updateSql = "UPDATE intervenant SET nom=?nom , tauxHoraire=?tauxHoraire WHERE id=?id";

        private static string _insertSql = "INSERT INTO intervenant (nom, tauxHoraire) VALUES (?nom, ?tauxHoraire)";

        private static string _deleteByIdSql = "DELETE FROM intervenant WHERE id=?id";

        private static string _getLastInsertId = "SELECT idIntervenant FROM intervenant WHERE nom=?nom AND tauxHoraire=?tauxHoraire";

        #endregion

        #region Méthodes d'accès aux données

        public static Intervenant Fetch(int idIntervenant)
        {
            Intervenant i = null;
            DataBaseAccess.Connexion.Open();
            MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand(); // Initialisation d'un objet qui permet d'interroger la base de donnée.
            commandSql.CommandText = _selectByIdSql; // Définit la requête à utiliser.
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?idIntervenant", idIntervenant)); //Transmet en paramètre à utiliser lors de l'envoi de la requête.
            commandSql.Prepare();
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();

            bool existeEnregistrement = jeuEnregistrements.Read(); // Lecture de l'enregistrement.
            if (existeEnregistrement)
            {
                i = new Intervenant();
                i._idIntervenant = Convert.ToInt16(jeuEnregistrements["idIntervenant"].ToString());
                i._nom = (jeuEnregistrements["nom"].ToString());
                i._tauxHoraire = Convert.ToDecimal(jeuEnregistrements["tauxHoraire"].ToString());
            }
            DataBaseAccess.Connexion.Close();
            return i;
        }

        public void Save()
        {
            if (_idIntervenant == -1)
            {
                Insert();
            }

            else
            {
                Update();
            }
        }

        public void Delete()
        {
            DataBaseAccess.Connexion.Open();
            MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _deleteByIdSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?idIntervenant", _idIntervenant));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            _idIntervenant = -1;
        }

        public void Update()
        {
            DataBaseAccess.Connexion.Open();
            MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _updateSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?idIntervenant", _idIntervenant));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?nom", _nom));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?tauxHoraire", _tauxHoraire));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();
            DataBaseAccess.Connexion.Close();
        }

        public void Insert()
        {
            DataBaseAccess.Connexion.Open();
            MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _insertSql;
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?idIntervenant", _idIntervenant));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?nom", _nom));
            commandSql.Parameters.Add(DataBaseAccess.CodeParam("?tauxHoraire", _tauxHoraire));
            commandSql.Prepare();
            commandSql.ExecuteNonQuery();

            MySqlCommand commandGetLastId = DataBaseAccess.Connexion.CreateCommand();
            commandGetLastId.CommandText = _getLastInsertId;
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?idIntervenant", _idIntervenant));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?nom", _nom));
            commandGetLastId.Parameters.Add(DataBaseAccess.CodeParam("?tauxHoraire", _tauxHoraire));
            commandGetLastId.Prepare();
            MySqlDataReader jeuEnregistrements = commandGetLastId.ExecuteReader();

            bool existEnregistrement = jeuEnregistrements.Read();
            if(existEnregistrement)
            {
                _idIntervenant = Convert.ToInt16(jeuEnregistrements["idIntervenant"].ToString());
            }
            else
            {
                commandSql.Transaction.Rollback();
                throw new Exception("Duplicate entry");
            }
            DataBaseAccess.Connexion.Close();
        }

        public static List<Intervenant> FetchAll()
        {
            List<Intervenant> resultat = new List<Intervenant>();
            DataBaseAccess.Connexion.Open();
            MySqlCommand commandSql = DataBaseAccess.Connexion.CreateCommand();
            commandSql.CommandText = _selectSql;
            MySqlDataReader jeuEnregistrements = commandSql.ExecuteReader();
            while (jeuEnregistrements.Read())
            {
                Intervenant unIntervenant = new Intervenant();
                string idIntervenant = jeuEnregistrements["idIntervenant"].ToString();
                unIntervenant._idIntervenant = Convert.ToInt16(idIntervenant);
                unIntervenant._nom = jeuEnregistrements["nom"].ToString();
                string tauxHoraire = jeuEnregistrements["tauxHoraire"].ToString();
                unIntervenant._tauxHoraire = Convert.ToDecimal(tauxHoraire);
                resultat.Add(unIntervenant);
            }
            DataBaseAccess.Connexion.Close();
            return resultat;


        }

        //public decimal getTauxHoraire()
        //{

        //    return 0;
        //}
    }
}